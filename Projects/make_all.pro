TEMPLATE = subdirs
CONFIG += ordered

PROJECTS = \
	EspressoLib \
	TransalgParser \
	TransalgFormats \
	TransalgInterpreter \
	Transalg

SUBDIRS = $$PROJECTS

message("Will build the following modules: $$SUBDIRS")
