#include "stdafx.h"
#include "DataTypeBit.h"
#include "DataTypeInteger.h"
#include "SemanticLibrary.h"

namespace Transalg
{

Integer::Integer(int value)
	: Void(Transalg::dtInt)
	, value_(value)
{
}

Integer& Integer::operator=(const Bit& rvalue)
{
	if(!rvalue.IsConst())
		throw std::runtime_error("Integer::operator=(const Bit& rvalue): rvalue is not const!");

	const LogicFormulaPtr& bit = rvalue.GetValue();
	assert(bit->IsConst());
	value_ = (bit->back().value.True() ? 1 : 0);
	return *this;
}

Integer& Integer::operator=(const BitVector& rvalue)
{
	//! Все биты вектора rvalue должны быть известны (булевы константы)
	if(!rvalue.IsConst())
		throw std::runtime_error("Integer::operator=(const Bit& rvalue): rvalue is not const!");

	value_ = 0;
	const std::size_t size = std::min(rvalue.GetSize(), sizeof(int));
	for(unsigned i = 0; i < size; ++i)
	{
		const LogicFormulaPtr& bit = rvalue[i]->GetValue();
		assert(bit->IsConst());
		if(bit->back().value.True())
			value_ |= (1 << i);
	}
	return *this;
}

Integer& Integer::operator=(const Integer& rvalue)
{
	value_ = rvalue.value_;
	return *this;
}

std::string Integer::ToString() const
{
	return std::string("[Integer]: ") + boost::lexical_cast<std::string>(value_);
}

} // namespace Transalg
