#pragma once

#include <stdexcept>
#include <cassert>
#include <vector>
#include <map>
#include <set>
#include <stack>
#include <string>
#include <fstream>
#include <iostream>
#include <sstream>
#include <algorithm>

#include <boost/shared_ptr.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/lexical_cast.hpp>
