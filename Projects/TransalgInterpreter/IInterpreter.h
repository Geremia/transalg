#pragma once

#include <TransalgParser/Types.h>
#include <TransalgFormats/EncodeFactory.h>

namespace Transalg
{

class IInterpreter
{
public:

	virtual ~IInterpreter() {}

	//! Пропозициональное кодирование ТА-программы
	virtual bool TranslateProgram(const StatementBlockPtr& program, EncodeFactoryPtr encode) = 0;

};

typedef boost::shared_ptr<IInterpreter> IInterpreterPtr;

} // namespace Transalg
