#pragma once

#include "IInterpreter.h"

namespace Transalg
{

class Interpreter: public IInterpreter
{
public:

	Interpreter();

	//! Пропозициональное кодирование ТА-программы
	bool TranslateProgram(const StatementBlockPtr& program, EncodeFactoryPtr encode);

private:

	Interpreter(const Interpreter&);
	Interpreter& operator=(const Interpreter&);

	class Impl;
	boost::shared_ptr<Impl> impl_;
};

} // namespace Transalg
