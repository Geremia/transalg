#pragma once

#include "Types.h"
#include "Statement.h"

namespace Transalg
{

/*
	Идентификатор + '(' - это функция.
	Идентификатор + '[' - это обращение к массиву.
	В этих случаях с начала разбираются выражения в аргументах, подсчитывается их число,
	и только потом в выходную последовательность добавляется идентификатор массива/функции с указанием числа аргрументов.

	Пример.

	Исходное выражение:

	a + b * c[i + 5][1] + f(x, y + z*z)

	ПОЛИЗ:

	abi5+1c*+xyzz*+f+

	// c -> arg_count_ == 2
	// f -> arg_count_ == 2
*/

//! Тип операнда/оператора в выражении
enum OperandType
{
	//! Константа
	OperandTypeConst,
	//! Переменная
	OperandTypeVariable,
	//! Элемент массива
	OperandTypeArrayVariable,
	//! Оператор
	OperandTypeOperator,
	//! Вызов функции
	OperandTypeFunction
};

struct ExpressionOperand
{
	ExpressionOperand(OperandType type, const std::string& id, unsigned arg_count = 0, unsigned line = 0)
		: type_(type)
		, id_(id)
		, arg_count_(arg_count)
		, line_(line)
	{}

	//! Тип операнда
	OperandType type_;

	//! Символьный идентификатор операнда
	std::string id_;

	//! Номер строки в исходном файле
	unsigned line_;

	//! Число аргументов, которые требует данная операция/функция
	//! Для массивов указывается число индексов
	//! У переменных и констант arg_count_ == 0
	unsigned arg_count_;
};

typedef std::vector<ExpressionOperand> ExpressionOperandContainer;
typedef boost::shared_ptr<ExpressionOperandContainer> ExpressionOperandContainerPtr;

class Expression: public Statement
{
	friend class Parser;
public:

	explicit Expression(const StatementBlock* context, unsigned line)
		: Statement(context, stmtExpression, line)
	{}

	std::size_t GetOperandsCount() const;

	const ExpressionOperand& GetOperand(unsigned index) const;

	friend std::ostream& operator<<(std::ostream& out, const Expression& e);

protected:
	//! Область видимости, к которой относится данное выражение
	//! Храним в виде ПОЛИЗ (обычная последовательность)
	ExpressionOperandContainer operands_;
};

typedef boost::shared_ptr<Expression> ExpressionPtr;

} // namespace Transalg
