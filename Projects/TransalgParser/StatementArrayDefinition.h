#pragma once

#include "Types.h"
#include "Statement.h"
#include "Expression.h"

namespace Transalg
{

class StatementArrayDefinition: public Statement
{
	friend class Parser;
public:
	
	StatementArrayDefinition(const StatementBlock* context, unsigned line, 
		Attribute attr, DataType type, const std::string& id)
		: Statement(context, stmtArrayDefinition, line)
		, attribute_(attr)
		, data_type_(type)
		, id_(id)
	{}

	Attribute GetAttribute() const;

	DataType GetDataType() const;

	const std::string& GetId() const;

	std::size_t GetIndexCount() const;

	unsigned GetIndex(unsigned index) const;

	std::size_t GetInitValueCount() const;

	unsigned GetInitValue(unsigned value_index) const;

	ExpressionPtr GetInitExpression() const;

private:

	//! Атрибут {__in, __out, __mem}
	Attribute attribute_;
	//! Тип данных переменной
	DataType data_type_;
	//! Идентификатор переменной
	std::string id_;
	//! Список индексов - целочисленные константы
	std::vector<unsigned> array_idxs_;
	//! Инициализация массивом целочисленных констант
	std::vector<unsigned> init_values_;
	//! Инициализация выражением
	ExpressionPtr init_expression_;
};

typedef boost::shared_ptr<StatementArrayDefinition> StatementArrayDefinitionPtr;

} // namespace Transalg
