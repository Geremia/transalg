#include "../Expression.h"

namespace Transalg
{

std::size_t Expression::GetOperandsCount() const
{
	return operands_.size();
}

const ExpressionOperand& Expression::GetOperand(unsigned index) const
{
	return operands_.at(index);
}

std::ostream& operator<<(std::ostream& out, const Expression& e)
{
	for(std::size_t i(0); i < e.operands_.size(); ++i)
	{
		out << (i==0?std::string(""):std::string(" ")) << (e.operands_[i].id_);
	}
	return out;
}

} // namespace Transalg
