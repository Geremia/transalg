#include "stdafx.h"
#include "../StatementFor.h"

namespace Transalg
{

StatementPtr StatementFor::GetInitStatement() const
{
	return init_;
}

ExpressionPtr StatementFor::GetStepExpression() const
{
	return step_;
}

ExpressionPtr StatementFor::GetConditionExpression() const
{
	return condition_;
}

StatementPtr StatementFor::GetBody() const
{
	return body_;
}

} // namespace Transalg
