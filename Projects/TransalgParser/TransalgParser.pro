TEMPLATE = lib
TARGET = TransalgParser

include(../../Mk/Common/common.pri)

CONFIG += staticlib

CONFIG(debug, debug|release) {
	OBJECTS_DIR += "$$BASEPATH/$$OBJ_FOLDER/$$DEBUG_FOLDER/$$TARGET"
	DESTDIR = "$$BASEPATH/$$LIB_FOLDER/$$DEBUG_FOLDER"
	LIBS += -L"$$BASEPATH/$$LIB_FOLDER/$$DEBUG_FOLDER"
} else {
	OBJECTS_DIR += "$$BASEPATH/$$OBJ_FOLDER/$$RELEASE_FOLDER/$$TARGET"
	DESTDIR = "$$BASEPATH/$$LIB_FOLDER/$$RELEASE_FOLDER"
	LIBS += -L"$$BASEPATH/$$LIB_FOLDER/$$RELEASE_FOLDER"
}

# Дополнительные каталоги включения для данного проекта
INCLUDEPATH += src . ..

PRECOMPILED_HEADER = src/stdafx.h
PRECOMPILED_SOURCE = src/stdafx.cpp

HEADERS += \
	Expression.h \
	ParseError.h \
	Parser.h \
	Serializer.h \
	Statement.h \
	StatementArrayDefinition.h \
	StatementBlock.h \
	StatementFor.h \
	StatementFunctionDefinition.h \
	StatementIf.h \
	StatementReturn.h \
	StatementTable.h \
	StatementVarDefinition.h \
	Types.h \
	src/Lexer.h

SOURCES += \
	src/Expression.cpp \
	src/Lexer.cpp \
	src/ParseError.cpp \
	src/Parser.cpp \
	src/Serializer.cpp \
	src/StatementArrayDefinition.cpp \
	src/StatementFor.cpp \
	src/StatementFunctionDefinition.cpp \
	src/StatementIf.cpp \
	src/StatementReturn.cpp \
	src/StatementTable.cpp \
	src/StatementVarDefinition.cpp
