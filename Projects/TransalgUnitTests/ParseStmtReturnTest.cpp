#include "TestMacros.h"

namespace
{

Transalg::ExpressionPtr ParseExpression(const std::string& text)
{
	using namespace Transalg;
	Parser parser;
	StatementBlockPtr program_block = parser.ParseProgramText(text);
	EXPECT_EQ(1, program_block->GetStatementsCount());
	StatementContainer::const_iterator stmt_it = program_block->begin();
	EXPECT_EQ(Transalg::stmtReturn, (*stmt_it)->GetStatementType());
	StatementReturnPtr ptr = boost::dynamic_pointer_cast<StatementReturn>(*stmt_it);
	EXPECT_TRUE(ptr);
	ExpressionPtr expr_ptr = ptr->GetReturnExpression();
	EXPECT_TRUE(expr_ptr);
	return expr_ptr;
}

} // local namespace

TEST(ParserTests, ParseStmtReturnTest1)
{
	using namespace Transalg;
	const std::string text("return x;");
	ExpressionPtr expr_ptr = ParseExpression(text);
	EXPECT_EQ(1, expr_ptr->GetOperandsCount());
	const ExpressionOperand& op = expr_ptr->GetOperand(0);
	EXPECT_EQ(0, op.arg_count_);
	EXPECT_EQ(Transalg::OperandTypeVariable, op.type_);
	EXPECT_EQ(1, op.line_);
	EXPECT_EQ("x", op.id_);
}

TEST(ParserTests, ParseStmtReturnTest2)
{
	using namespace Transalg;
	const std::string text("return 1;");
	ExpressionPtr expr_ptr = ParseExpression(text);
	EXPECT_EQ(1, expr_ptr->GetOperandsCount());
	const ExpressionOperand& op = expr_ptr->GetOperand(0);
	EXPECT_EQ(0, op.arg_count_);
	EXPECT_EQ(Transalg::OperandTypeConst, op.type_);
	EXPECT_EQ(1, op.line_);
	EXPECT_EQ("1", op.id_);
}

TEST(ParserTests, ParseStmtReturnTest3)
{
	using namespace Transalg;
	const std::string text("return x & y(z, 1);");
	ExpressionPtr expr_ptr = ParseExpression(text);
	EXPECT_EQ(5, expr_ptr->GetOperandsCount());
	
	//! Внутренний порядок операндов: x z 1 y &
	{
		const ExpressionOperand& op = expr_ptr->GetOperand(0);
		EXPECT_EQ(0, op.arg_count_);
		EXPECT_EQ(Transalg::OperandTypeVariable, op.type_);
		EXPECT_EQ(1, op.line_);
		EXPECT_EQ("x", op.id_);
	}
	{
		const ExpressionOperand& op = expr_ptr->GetOperand(1);
		EXPECT_EQ(0, op.arg_count_);
		EXPECT_EQ(Transalg::OperandTypeVariable, op.type_);
		EXPECT_EQ(1, op.line_);
		EXPECT_EQ("z", op.id_);
	}
	{
		const ExpressionOperand& op = expr_ptr->GetOperand(2);
		EXPECT_EQ(0, op.arg_count_);
		EXPECT_EQ(Transalg::OperandTypeConst, op.type_);
		EXPECT_EQ(1, op.line_);
		EXPECT_EQ("1", op.id_);
	}
	{
		const ExpressionOperand& op = expr_ptr->GetOperand(3);
		EXPECT_EQ(2, op.arg_count_);
		EXPECT_EQ(Transalg::OperandTypeFunction, op.type_);
		EXPECT_EQ(1, op.line_);
		EXPECT_EQ("y", op.id_);
	}
	{
		const ExpressionOperand& op = expr_ptr->GetOperand(4);
		EXPECT_EQ(2, op.arg_count_);
		EXPECT_EQ(Transalg::OperandTypeOperator, op.type_);
		EXPECT_EQ(1, op.line_);
		EXPECT_EQ("&", op.id_);
	}
}

TEST(ParserTests, ParseStmtReturnTest4)
{
	using namespace Transalg;
	const std::string text("return x[10][2] + 1;");
	ExpressionPtr expr_ptr = ParseExpression(text);
	EXPECT_EQ(5, expr_ptr->GetOperandsCount());
	//! Внутренний порядок операндов: 10 2 x 1 +
	{
		const ExpressionOperand& op = expr_ptr->GetOperand(0);
		EXPECT_EQ(0, op.arg_count_);
		EXPECT_EQ(Transalg::OperandTypeConst, op.type_);
		EXPECT_EQ(1, op.line_);
		EXPECT_EQ("10", op.id_);
	}
	{
		const ExpressionOperand& op = expr_ptr->GetOperand(1);
		EXPECT_EQ(0, op.arg_count_);
		EXPECT_EQ(Transalg::OperandTypeConst, op.type_);
		EXPECT_EQ(1, op.line_);
		EXPECT_EQ("2", op.id_);
	}
	{
		const ExpressionOperand& op = expr_ptr->GetOperand(2);
		EXPECT_EQ(2, op.arg_count_);
		EXPECT_EQ(Transalg::OperandTypeArrayVariable, op.type_);
		EXPECT_EQ(1, op.line_);
		EXPECT_EQ("x", op.id_);
	}
	{
		const ExpressionOperand& op = expr_ptr->GetOperand(3);
		EXPECT_EQ(0, op.arg_count_);
		EXPECT_EQ(Transalg::OperandTypeConst, op.type_);
		EXPECT_EQ(1, op.line_);
		EXPECT_EQ("1", op.id_);
	}
	{
		const ExpressionOperand& op = expr_ptr->GetOperand(4);
		EXPECT_EQ(2, op.arg_count_);
		EXPECT_EQ(Transalg::OperandTypeOperator, op.type_);
		EXPECT_EQ(1, op.line_);
		EXPECT_EQ("+", op.id_);
	}
}

