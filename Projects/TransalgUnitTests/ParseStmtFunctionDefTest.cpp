#include "TestMacros.h"

namespace
{

Transalg::StatementFunctionDefinitionPtr
ParseFunctionDefinition(const std::string& text)
{
	using namespace Transalg;
	Parser parser;
	StatementBlockPtr program = parser.ParseProgramText(text);
	EXPECT_EQ(1, program->GetStatementsCount());
	return boost::dynamic_pointer_cast<StatementFunctionDefinition>(*(program->begin()));
}

}

// Функция, которая ничего не делает
TEST(ParserTests, ParseFunctionDefTest1)
{
	using namespace Transalg;
	const std::string text(
		"void main(){}"
	);

	StatementFunctionDefinitionPtr ptr = ParseFunctionDefinition(text);
	TestFunctionDefinition(ptr, Transalg::dtVoid, "main", 0);
	StatementBlockPtr body = ptr->GetBody();
	EXPECT_TRUE(body);
	EXPECT_EQ(0, body->GetStatementsCount());
}

// Функция возвращает константу целого типа
TEST(ParserTests, ParseFunctionDefTest2)
{
	using namespace Transalg;
	const std::string text(
		"int GetSize()\n"
		"{\n"
		"	return 1;\n"
		"}"
	);
	StatementFunctionDefinitionPtr ptr = ParseFunctionDefinition(text);
	TestFunctionDefinition(ptr, Transalg::dtInt, "GetSize", 0);
	StatementBlockPtr body = ptr->GetBody();
	EXPECT_TRUE(body);
	EXPECT_EQ(1, body->GetStatementsCount());
	// return
	StatementReturnPtr ret = boost::dynamic_pointer_cast<StatementReturn>(*(body->begin()));
	EXPECT_TRUE(ret);
	ExpressionPtr ret_expr = ret->GetReturnExpression();
	TestExpression(ret_expr, 1);
	TestExpressionOperand(ret_expr->GetOperand(0), Transalg::OperandTypeConst, 0, "1", 3);
}

// Функция принимает два целочисленных параметра и возвращает их сумму
TEST(ParserTests, ParseFunctionDefTest3)
{
	using namespace Transalg;
	const std::string text(
		"int Sum(int x, int y)\n"
		"{return x + y;}"
	);
	StatementBlockPtr body;
	// signature
	{
		StatementFunctionDefinitionPtr ptr = ParseFunctionDefinition(text);
		TestFunctionDefinition(ptr, Transalg::dtInt, "Sum", 2);
		std::vector<unsigned> indexes;
		TestFunctionArgument(ptr, 0, Transalg::dtInt, "x", indexes);
		TestFunctionArgument(ptr, 1, Transalg::dtInt, "y", indexes);
		body = ptr->GetBody();
	}
	// return
	{
		EXPECT_TRUE(body);
		EXPECT_EQ(1, body->GetStatementsCount());
		StatementReturnPtr ret = boost::dynamic_pointer_cast<StatementReturn>(*(body->begin()));
		EXPECT_TRUE(ret);
		ExpressionPtr ret_expr = ret->GetReturnExpression();
		TestExpression(ret_expr, 3);
		TestExpressionOperand(ret_expr->GetOperand(0), Transalg::OperandTypeVariable, 0, "x", 2);
		TestExpressionOperand(ret_expr->GetOperand(1), Transalg::OperandTypeVariable, 0, "y", 2);
		TestExpressionOperand(ret_expr->GetOperand(2), Transalg::OperandTypeOperator, 2, "+", 2);
	}
}

// Мажоритарная функция (используется в генераторах ключевого потока)
TEST(ParserTests, ParseFunctionDefTest4)
{
	using namespace Transalg;
	const std::string text(
		"bit majority(bit x, bit y, bit z)\n"
		"{\n"
		"	return x&y | x&z | y&z;\n"
		"}"
	);
	StatementBlockPtr body;
	// signature
	{
		StatementFunctionDefinitionPtr ptr = ParseFunctionDefinition(text);
		TestFunctionDefinition(ptr, Transalg::dtBit, "majority", 3);
		std::vector<unsigned> indexes;
		TestFunctionArgument(ptr, 0, Transalg::dtBit, "x", indexes);
		TestFunctionArgument(ptr, 1, Transalg::dtBit, "y", indexes);
		TestFunctionArgument(ptr, 2, Transalg::dtBit, "z", indexes);
		body = ptr->GetBody();
	}
	// return
	{
		EXPECT_TRUE(body);
		EXPECT_EQ(1, body->GetStatementsCount());
		StatementReturnPtr ret = boost::dynamic_pointer_cast<StatementReturn>(*(body->begin()));
		EXPECT_TRUE(ret);
		// x y & x z & | y z & |
		ExpressionPtr ret_expr = ret->GetReturnExpression();
		TestExpression(ret_expr, 11);
		TestExpressionOperand(ret_expr->GetOperand(0), Transalg::OperandTypeVariable, 0, "x", 3);
		TestExpressionOperand(ret_expr->GetOperand(1), Transalg::OperandTypeVariable, 0, "y", 3);
		TestExpressionOperand(ret_expr->GetOperand(2), Transalg::OperandTypeOperator, 2, "&", 3);
		TestExpressionOperand(ret_expr->GetOperand(3), Transalg::OperandTypeVariable, 0, "x", 3);
		TestExpressionOperand(ret_expr->GetOperand(4), Transalg::OperandTypeVariable, 0, "z", 3);
		TestExpressionOperand(ret_expr->GetOperand(5), Transalg::OperandTypeOperator, 2, "&", 3);
		TestExpressionOperand(ret_expr->GetOperand(6), Transalg::OperandTypeOperator, 2, "|", 3);
		TestExpressionOperand(ret_expr->GetOperand(7), Transalg::OperandTypeVariable, 0, "y", 3);
		TestExpressionOperand(ret_expr->GetOperand(8), Transalg::OperandTypeVariable, 0, "z", 3);
		TestExpressionOperand(ret_expr->GetOperand(9), Transalg::OperandTypeOperator, 2, "&", 3);
		TestExpressionOperand(ret_expr->GetOperand(10), Transalg::OperandTypeOperator, 2, "|", 3);
	}
}

// РСЛОС
TEST(ParserTests, ParseFunctionDefTest5)
{
	using namespace Transalg;
	const std::string text(
		"void shift_rslos()\n"
		"{\n"
		"	bit y = reg[18]^reg[17]^reg[16]^reg[13];\n"
		"	for(int j = 18; j > 0; j = j - 1){\n"
		"		reg[j] = reg[j-1];\n"
		"	}\n"
		"	reg[0] = y;\n"
		"}"
	);
	StatementBlockPtr body;
	// signature
	{
		StatementFunctionDefinitionPtr ptr = ParseFunctionDefinition(text);
		TestFunctionDefinition(ptr, Transalg::dtVoid, "shift_rslos", 0);
		body = ptr->GetBody();
	}
	EXPECT_TRUE(body);
	EXPECT_EQ(3, body->GetStatementsCount());
	// var def
	{
		StatementVarDefinitionPtr stmt = boost::dynamic_pointer_cast<StatementVarDefinition>(*(body->begin()));
		TestVarDefinition(stmt, Transalg::AttributeNone, Transalg::dtBit, "y");
		// 18 reg 17 reg ^ 16 reg ^ 13 reg ^
		ExpressionPtr init = stmt->GetInitValue();
		TestExpression(init, 11);
		TestExpressionOperand(init->GetOperand(0), Transalg::OperandTypeConst, 0, "18", 3);
		TestExpressionOperand(init->GetOperand(1), Transalg::OperandTypeArrayVariable, 1, "reg", 3);
		TestExpressionOperand(init->GetOperand(2), Transalg::OperandTypeConst, 0, "17", 3);
		TestExpressionOperand(init->GetOperand(3), Transalg::OperandTypeArrayVariable, 1, "reg", 3);
		TestExpressionOperand(init->GetOperand(4), Transalg::OperandTypeOperator, 2, "^", 3);
		TestExpressionOperand(init->GetOperand(5), Transalg::OperandTypeConst, 0, "16", 3);
		TestExpressionOperand(init->GetOperand(6), Transalg::OperandTypeArrayVariable, 1, "reg", 3);
		TestExpressionOperand(init->GetOperand(7), Transalg::OperandTypeOperator, 2, "^", 3);
		TestExpressionOperand(init->GetOperand(8), Transalg::OperandTypeConst, 0, "13", 3);
		TestExpressionOperand(init->GetOperand(9), Transalg::OperandTypeArrayVariable, 1, "reg", 3);
		TestExpressionOperand(init->GetOperand(10), Transalg::OperandTypeOperator, 2, "^", 3);
	}
	// for loop
	{
		StatementForPtr stmt = boost::dynamic_pointer_cast<StatementFor>(*(body->begin() + 1));
		EXPECT_TRUE(stmt);
		EXPECT_TRUE(stmt->GetInitStatement());
		EXPECT_TRUE(stmt->GetConditionExpression());
		EXPECT_TRUE(stmt->GetStepExpression());
		EXPECT_TRUE(stmt->GetBody());
	}
	// assignment
	{
		ExpressionPtr expr = boost::dynamic_pointer_cast<Expression>(*(body->begin() + 2));
		// 0 reg y =
		TestExpression(expr, 4);
		TestExpressionOperand(expr->GetOperand(0), Transalg::OperandTypeConst, 0, "0", 7);
		TestExpressionOperand(expr->GetOperand(1), Transalg::OperandTypeArrayVariable, 1, "reg", 7);
		TestExpressionOperand(expr->GetOperand(2), Transalg::OperandTypeVariable, 0, "y", 7);
		TestExpressionOperand(expr->GetOperand(3), Transalg::OperandTypeOperator, 2, "=", 7);
	}
}

// Функция принимающая на вход массивы бит (нелинейная функция из MD5)
TEST(ParserTests, ParseFunctionDefTest6)
{
	using namespace Transalg;
	const std::string text(
		"bit F(bit X[32], bit Y[32], bit Z[32])\n"
		"{\n"
		"	return (X&Y)|(!X&Z);\n"
		"}"
	);
	StatementBlockPtr body;
	// signature
	{
		StatementFunctionDefinitionPtr ptr = ParseFunctionDefinition(text);
		TestFunctionDefinition(ptr, Transalg::dtBit, "F", 3);
		std::vector<unsigned> indexes;
		indexes.push_back(32);
		TestFunctionArgument(ptr, 0, Transalg::dtBit, "X", indexes);
		TestFunctionArgument(ptr, 1, Transalg::dtBit, "Y", indexes);
		TestFunctionArgument(ptr, 2, Transalg::dtBit, "Z", indexes);
		body = ptr->GetBody();
	}
	EXPECT_TRUE(body);
	EXPECT_EQ(1, body->GetStatementsCount());
	// return
	{
		StatementReturnPtr ret = boost::dynamic_pointer_cast<StatementReturn>(*(body->begin()));
		EXPECT_TRUE(ret);
		ExpressionPtr expr = ret->GetReturnExpression();
		// X Y & X ! Z & |
		TestExpression(expr, 8);
		TestExpressionOperand(expr->GetOperand(0), Transalg::OperandTypeVariable, 0, "X", 3);
		TestExpressionOperand(expr->GetOperand(1), Transalg::OperandTypeVariable, 0, "Y", 3);
		TestExpressionOperand(expr->GetOperand(2), Transalg::OperandTypeOperator, 2, "&", 3);
		TestExpressionOperand(expr->GetOperand(3), Transalg::OperandTypeVariable, 0, "X", 3);
		TestExpressionOperand(expr->GetOperand(4), Transalg::OperandTypeOperator, 1, "!", 3);
		TestExpressionOperand(expr->GetOperand(5), Transalg::OperandTypeVariable, 0, "Z", 3);
		TestExpressionOperand(expr->GetOperand(6), Transalg::OperandTypeOperator, 2, "&", 3);
		TestExpressionOperand(expr->GetOperand(7), Transalg::OperandTypeOperator, 2, "|", 3);
	}
}
