#pragma once

#include <stdexcept>
#include <cassert>
#include <vector>
#include <map>
#include <set>
#include <stack>
#include <string>
#include <fstream>
#include <sstream>
#include <algorithm>

#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/lexical_cast.hpp>

#include <gtest/gtest.h>
#include <TransalgInterpreter/src/SemanticLibrary.h>
//#include <TransalgInterpreter/src/EncodeFactory.h>
#include <TransalgInterpreter/src/DataTypes.h>
#include <TransalgInterpreter/src/DataTypeBit.h>
#include <TransalgInterpreter/src/DataTypeInteger.h>
#include <TransalgFormats/EncodeFactory.h>
#include <TransalgFormats/LogicEquationSerializer.h>
