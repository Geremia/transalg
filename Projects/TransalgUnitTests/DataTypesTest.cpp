#include "stdafx.h"

TEST(DataTypes, BitVectorConstructor)
{
	using namespace Transalg;

	const Bit bit_false(EncodeFactory::False());
	const Bit bit_true(EncodeFactory::True());
	//! Создание вектора указанного размера, иниц. нулями
	{
		const std::size_t size(10);
		BitVector vec(10);
		EXPECT_EQ(size, vec.GetSize());
		for(unsigned i = 0; i < size; ++i)
		{
			EXPECT_EQ(bit_false, *vec[i]);
		}
	}

	//! Создание вектора из строкового литерала, содержащего 16-ричное число
	{
		BitVector vec("0x5");
		EXPECT_EQ(3, vec.GetSize());
		EXPECT_EQ(bit_true, *vec[0]);
		EXPECT_EQ(bit_false, *vec[1]);
		EXPECT_EQ(bit_true, *vec[2]);
	}
	{
		BitVector vec("0xffff");
		EXPECT_EQ(16, vec.GetSize());
		for(unsigned i = 0; i < vec.GetSize(); ++i)
			EXPECT_EQ(bit_true, *vec[i]);
	}
	{
		BitVector vec("0x0000");
		EXPECT_EQ(1, vec.GetSize());
		EXPECT_EQ(bit_false, *vec[0]);
	}
	{
		BitVector vec("0x3fffffffff");
		EXPECT_EQ(38, vec.GetSize());
		for(unsigned i = 0; i < vec.GetSize(); ++i)
			EXPECT_EQ(bit_true, *vec[i]);
	}

	//! Создание вектора из списка формул
	{
		LogicFormulaContainer lf;
		lf.push_back(EncodeFactory::False());
		lf.push_back(EncodeFactory::True());
		lf.push_back(EncodeFactory::False());
		lf.push_back(EncodeFactory::True());
		lf.push_back(EncodeFactory::False());

		//? Старшиный нулевой бит не удаляется
		BitVector vec(lf);
		EXPECT_EQ(5, vec.GetSize());
		EXPECT_EQ(bit_false, *vec[0]);
		EXPECT_EQ(bit_true, *vec[1]);
		EXPECT_EQ(bit_false, *vec[2]);
		EXPECT_EQ(bit_true, *vec[3]);
		EXPECT_EQ(bit_false, *vec[4]);
	}

	//! Создание вектора из списка бит
	{
		BitContainer bits;
		bits.push_back(boost::make_shared<Bit>(EncodeFactory::True()));
		bits.push_back(boost::make_shared<Bit>(EncodeFactory::True()));
		bits.push_back(boost::make_shared<Bit>(EncodeFactory::False()));
		bits.push_back(boost::make_shared<Bit>(EncodeFactory::True()));
		bits.push_back(boost::make_shared<Bit>(EncodeFactory::False()));

		BitVector vec(bits);
		EXPECT_EQ(5, vec.GetSize());
		EXPECT_EQ(bit_true, *vec[0]);
		EXPECT_EQ(bit_true, *vec[1]);
		EXPECT_EQ(bit_false, *vec[2]);
		EXPECT_EQ(bit_true, *vec[3]);
		EXPECT_EQ(bit_false, *vec[4]);
	}
}

TEST(DataTypes, BitVectorCompare)
{
	using namespace Transalg;
	//! Проверка на равенство
	{
		BitVector vec1("0x5");
		BitVector vec2("0x5");
		EXPECT_TRUE(vec1 == vec2);
		EXPECT_EQ(vec1, vec2);
	}
	//! Проверка на неравенство
	{
		BitVector vec1("0x5");
		BitVector vec2("0xf");
		EXPECT_FALSE(vec1 == vec2);
	}
}
