#include "TestMacros.h"

// Объявление счетчика цикла внутри цикла.
// Тело цикла - одна инструкция.
TEST(ParserTests, ParseStmtForTest1)
{
	using namespace Transalg;
	const std::string text(
		"int x[10];\n"
		"for(int i = 0; i < 10; i = i + 1)\n"
		"	x[i] = i;"
	);

	Parser parser;
	StatementBlockPtr program = parser.ParseProgramText(text);
	EXPECT_EQ(2, program->GetStatementsCount());
	//! Объявление массива x
	{
		StatementPtr stmt = *(program->begin());
		EXPECT_EQ(Transalg::stmtArrayDefinition, stmt->GetStatementType());
		StatementArrayDefinitionPtr arr_stmt = boost::dynamic_pointer_cast<StatementArrayDefinition>(stmt);
		TestArrayDefinition(arr_stmt, Transalg::AttributeNone, Transalg::dtInt, "x", 1, 0);
		EXPECT_EQ(10, arr_stmt->GetIndex(0));
	}
	//! Цикл for
	{
		StatementPtr stmt = *(program->begin() + 1);
		EXPECT_EQ(Transalg::stmtFor, stmt->GetStatementType());
		StatementForPtr for_stmt = boost::dynamic_pointer_cast<StatementFor>(stmt);
		EXPECT_TRUE(for_stmt);
		//! Объявление и инициализация счетчика цикла
		StatementVarDefinitionPtr init = boost::dynamic_pointer_cast<StatementVarDefinition>(for_stmt->GetInitStatement());
		TestVarDefinition(init, Transalg::AttributeNone, Transalg::dtInt, "i");
		ExpressionPtr init_expr = init->GetInitValue();
		TestExpression(init_expr, 1);
		TestExpressionOperand(init_expr->GetOperand(0), Transalg::OperandTypeConst, 0, "0", 2);
		//! Условие цикла, i 10 <
		ExpressionPtr cond_expr = for_stmt->GetConditionExpression();
		TestExpression(cond_expr, 3);
		TestExpressionOperand(cond_expr->GetOperand(0), Transalg::OperandTypeVariable, 0, "i", 2);
		TestExpressionOperand(cond_expr->GetOperand(1), Transalg::OperandTypeConst, 0, "10", 2);
		TestExpressionOperand(cond_expr->GetOperand(2), Transalg::OperandTypeOperator, 2, "<", 2);
		//! Шаг цикла, i i 1 + =
		ExpressionPtr step_expr = for_stmt->GetStepExpression();
		TestExpression(step_expr, 5);
		TestExpressionOperand(step_expr->GetOperand(0), Transalg::OperandTypeVariable, 0, "i", 2);
		TestExpressionOperand(step_expr->GetOperand(1), Transalg::OperandTypeVariable, 0, "i", 2);
		TestExpressionOperand(step_expr->GetOperand(2), Transalg::OperandTypeConst, 0, "1", 2);
		TestExpressionOperand(step_expr->GetOperand(3), Transalg::OperandTypeOperator, 2, "+", 2);
		TestExpressionOperand(step_expr->GetOperand(4), Transalg::OperandTypeOperator, 2, "=", 2);
		//! Тело цикла, i x i =
		ExpressionPtr body = boost::dynamic_pointer_cast<Expression>(for_stmt->GetBody());
		TestExpression(body, 4);
		TestExpressionOperand(body->GetOperand(0), Transalg::OperandTypeVariable, 0, "i", 3);
		TestExpressionOperand(body->GetOperand(1), Transalg::OperandTypeArrayVariable, 1, "x", 3);
		TestExpressionOperand(body->GetOperand(2), Transalg::OperandTypeVariable, 0, "i", 3);
		TestExpressionOperand(body->GetOperand(3), Transalg::OperandTypeOperator, 2, "=", 3);
	}
}

// Объявление счетчика цикла перед циклом.
// Инициализация счетчика и шаг цикла - необязательные
// Тело цикла - составной оператор.
TEST(ParserTests, ParseStmtForTest2)
{
	using namespace Transalg;
	const std::string text(
		"int i = 0;\n"
		"for(; i != 3; )\n"
		"{\n"
		"	i = i + 1;\n"
		"}"
	);
	Parser parser;
	StatementBlockPtr program = parser.ParseProgramText(text);
	EXPECT_EQ(2, program->GetStatementsCount());
	// Объявление переменной i
	{
		StatementPtr stmt = *(program->begin());
		StatementVarDefinitionPtr var_def = boost::dynamic_pointer_cast<StatementVarDefinition>(stmt);
		TestVarDefinition(var_def, Transalg::AttributeNone, Transalg::dtInt, "i");
		ExpressionPtr init = var_def->GetInitValue();
		TestExpression(init, 1);
		TestExpressionOperand(init->GetOperand(0), Transalg::OperandTypeConst, 0, "0", 1);
	}
	// Цикл for
	{
		StatementPtr stmt = *(program->begin() + 1);
		EXPECT_EQ(Transalg::stmtFor, stmt->GetStatementType());
		StatementForPtr for_stmt = boost::dynamic_pointer_cast<StatementFor>(stmt);
		EXPECT_TRUE(for_stmt);
		//! Объявление и инициализация счетчика цикла
		EXPECT_FALSE(for_stmt->GetInitStatement());
		//! Условие цикла, i 3 !=
		ExpressionPtr cond_expr = for_stmt->GetConditionExpression();
		TestExpression(cond_expr, 3);
		TestExpressionOperand(cond_expr->GetOperand(0), Transalg::OperandTypeVariable, 0, "i", 2);
		TestExpressionOperand(cond_expr->GetOperand(1), Transalg::OperandTypeConst, 0, "3", 2);
		TestExpressionOperand(cond_expr->GetOperand(2), Transalg::OperandTypeOperator, 2, "!=", 2);
		//! Шаг цикла
		// EXPECT_FALSE(for_stmt->GetStepExpression());
		//! Тело цикла, i i 1 + =
		StatementBlockPtr body = boost::dynamic_pointer_cast<StatementBlock>(for_stmt->GetBody());
		EXPECT_TRUE(body);
		EXPECT_EQ(1, body->GetStatementsCount());
		EXPECT_FALSE(body->GetFunction());
	}
}

// Использование именованной константы в условии цикла.
// Ветвление в теле цикла.
// define не образует инструкцию.
TEST(ParserTests, ParseStmtForTest3)
{
	using namespace Transalg;
	const std::string text(
		"define len 5;\n"
		"__in bit x[len];\n"
		"for(int i = 0; i < len; i = i + 1)\n"
		"{\n"
		"	if(x[i])\n"
		"		shift(x);\n"
		"}"
	);
	Parser parser;
	StatementBlockPtr program = parser.ParseProgramText(text);
	EXPECT_EQ(2, program->GetStatementsCount());
	// Объявление массива x
	{
		StatementPtr stmt = *(program->begin());
		EXPECT_EQ(Transalg::stmtArrayDefinition, stmt->GetStatementType());
		StatementArrayDefinitionPtr arr_stmt = boost::dynamic_pointer_cast<StatementArrayDefinition>(stmt);
		TestArrayDefinition(arr_stmt, Transalg::AttributeIn, Transalg::dtBit, "x", 1, 0);
		EXPECT_EQ(5, arr_stmt->GetIndex(0));
	}
	// Цикл for
	{
		StatementPtr stmt = *(program->begin() + 1);
		EXPECT_EQ(Transalg::stmtFor, stmt->GetStatementType());
		StatementForPtr for_stmt = boost::dynamic_pointer_cast<StatementFor>(stmt);
		EXPECT_TRUE(for_stmt);
		//! Объявление и инициализация счетчика цикла
		{
			StatementVarDefinitionPtr init = boost::dynamic_pointer_cast<StatementVarDefinition>(for_stmt->GetInitStatement());
			TestVarDefinition(init, Transalg::AttributeNone, Transalg::dtInt, "i");
			ExpressionPtr init_expr = init->GetInitValue();
			TestExpression(init_expr, 1);
			TestExpressionOperand(init_expr->GetOperand(0), Transalg::OperandTypeConst, 0, "0", 3);
		}
		//! Условие цикла, i 5 <
		{
			ExpressionPtr cond_expr = for_stmt->GetConditionExpression();
			TestExpression(cond_expr, 3);
			TestExpressionOperand(cond_expr->GetOperand(0), Transalg::OperandTypeVariable, 0, "i", 3);
			TestExpressionOperand(cond_expr->GetOperand(1), Transalg::OperandTypeConst, 0, "5", 3);
			TestExpressionOperand(cond_expr->GetOperand(2), Transalg::OperandTypeOperator, 2, "<", 3);
		}
		//! Шаг цикла, i i 1 + =
		{
			ExpressionPtr step_expr = for_stmt->GetStepExpression();
			TestExpression(step_expr, 5);
			TestExpressionOperand(step_expr->GetOperand(0), Transalg::OperandTypeVariable, 0, "i", 3);
			TestExpressionOperand(step_expr->GetOperand(1), Transalg::OperandTypeVariable, 0, "i", 3);
			TestExpressionOperand(step_expr->GetOperand(2), Transalg::OperandTypeConst, 0, "1", 3);
			TestExpressionOperand(step_expr->GetOperand(3), Transalg::OperandTypeOperator, 2, "+", 3);
			TestExpressionOperand(step_expr->GetOperand(4), Transalg::OperandTypeOperator, 2, "=", 3);
		}
		//! Тело цикла
		StatementBlockPtr block = boost::dynamic_pointer_cast<StatementBlock>(for_stmt->GetBody());
		EXPECT_EQ(1, block->GetStatementsCount());
		//! Условный оператор
		{
			StatementIfPtr if_stmt = boost::dynamic_pointer_cast<StatementIf>(*(block->begin()));
			EXPECT_TRUE(if_stmt);
			ExpressionPtr cond = if_stmt->GetCondition();
			TestExpression(cond, 2);
			TestExpressionOperand(cond->GetOperand(0), Transalg::OperandTypeVariable, 0, "i", 5);
			TestExpressionOperand(cond->GetOperand(1), Transalg::OperandTypeArrayVariable, 1, "x", 5);
			EXPECT_TRUE(if_stmt->GetTrueBranch());
			EXPECT_FALSE(if_stmt->GetFalseBranch());
		}
	}
}
