/************************************************************************************
* Transalg -- Copyright (c) 2015, Ilya Otpuschennikov
*
* This file is part of Transalg.
* 
* Transalg is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
* 
* Transalg is WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
**************************************************************************************/

#include <ctime>
#include <iostream>
#include <fstream>
#include <sstream>
#include <stack>

#include <boost/program_options.hpp>

#include <TransalgParser/Parser.h>
#include <TransalgParser/ParseError.h>

#include <TransalgInterpreter/Interpreter.h>
#include <TransalgFormats/OutputGenerator.h>
#include <TransalgFormats/EncodeFactory.h>
#include <TransalgFormats/VarLayers.h>

inline double cpuTime(void) { return (double)clock() / CLOCKS_PER_SEC; }

int main(int argc, char* argv[])
{
	namespace po = boost::program_options;

	po::options_description options("Usage: Transalg [options]\nAvailable options");
	options.add_options()
		("help,h", "display this help and exit")
		("input,i", po::value<std::string>(), "input file containing the TA-program")
		("output,o", po::value<std::string>(), "output file containing propositional encoding")
		("format,f", po::value<std::string>()->default_value("cnf"), "output format {cnf, ls}")
		("verbose", "enable output of additional info")
		("check-var-set-size", "check size of the sets of variables over which the formulas are constructed")
		("no-optimize", "disable optimization of the propositional encoding")
		("dump-layers", "dump variable layers")
		("dimacs-layers", "dump variable layers to DIMACS")
		("version,v", "output current program version")
		;

	try
	{
		double start_time(cpuTime());
		po::variables_map params;
		po::store(po::parse_command_line(argc, argv, options), params);

		if(params.count("help"))
		{
			std::cout << options;
			return 0;
		}

		if(params.count("version"))
		{
			std::cout << "Version: 1.1.5.0" << std::endl;
			std::cout << "Build date: " << __DATE__ << " " << __TIME__ << std::endl;
			return 0;
		}

		if(!params.count("input"))
		{
			throw std::invalid_argument("main: input file is not defined!");
		}

		if(!params.count("output"))
		{
			throw std::invalid_argument("main: output file is not defined!");
		}

		const std::string input_file = params["input"].as<std::string>();
		const std::string output_file = params["output"].as<std::string>();
		const std::string output_format = params["format"].as<std::string>();

		//! Пропозициональная кодировка
		Transalg::EncodeFactoryPtr encode = boost::make_shared<Transalg::EncodeFactory>();
		{
			//! Парсер ТА-программы
			Transalg::Parser parser;
			Transalg::StatementBlockPtr program_block = parser.ParseProgram(input_file);

			//! Интерпретатор ТА-программы
			Transalg::Interpreter interpreter;
			if(!interpreter.TranslateProgram(program_block, encode))
			{
				throw std::runtime_error("main: program translate is failed!");
			}
		}

		//! Проверка кодировки, удаление лишний переменных.
		//! Не выполняется, если задан ключ --no-optimize.
		if(!params.count("no-optimize"))
		{
			encode->CheckEncode(std::cout);
		}

		//! Перенумерация переменных
		encode->RenumberingVariables();

		std::string var_set_check_info;
		if(params.count("check-var-set-size"))
		{
			const std::string filename("check_var_set_size.log");
			std::ofstream log(filename.c_str(), std::ios::out);
			if(!log.is_open())
				throw std::runtime_error("main: can't open log file " + filename);
			var_set_check_info = encode->CheckVarSetSize(log);
		}

		if(params.count("verbose"))
		{
			std::cout << "Input   encode variable count: " << encode->input_vars.size() << std::endl;
			std::cout << "Tseitin encode variable count: " << encode->tseitin_vars.size() << std::endl;
			std::cout << "Output  encode variable count: " << encode->output_vars.size() << std::endl;
			std::cout << "Constraints count: " << encode->constraints.size() << std::endl;
			// std::cout << "Remove unused variables: " << encode->GetRemovedVariablesCount() << std::endl;
			if(params.count("check-var-set-size"))
			{
				std::cout << std::endl << "Check size of the sets of variables:" << std::endl;
				std::cout << var_set_check_info << std::endl;
			}
			std::cout << "Generation time of equation system: " << cpuTime() - start_time << " s." << std::endl;
			std::cout << "Begin generation of encoding..." << std::endl;
			start_time = cpuTime();
		}

		//! Определяем формат вывода: КНФ (по умолчанию) или система уравнений
		Transalg::OutputFormat format = Transalg::OutputFormatCnf;
		if(output_format == "ls")
			format = Transalg::OutputFormatLs;

		Transalg::OutputGenerator generator;
		generator.AddEncode(encode->input_vars);
		generator.AddEncode(encode->tseitin_vars);
		generator.AddEncode(encode->output_vars);
		generator.AddEncode(encode->constraints);
		generator.SetCoreVars(encode->core_vars);
		generator.ShowLayerVars(params.count("dimacs-layers") > 0);
		generator.Generate(output_file, format);

		if(params.count("verbose"))
		{
			std::cout << "End generation of encoding" << std::endl;
			if(output_format == "cnf")
			{
				std::cout << "Count of variables: " << generator.GetVarCount() << std::endl;
				std::cout << "Count of disjuncts: " << generator.GetDisjunctCount() << std::endl;
				std::cout << "Count of literals: " << generator.GetLiteralsCount() << std::endl;
			}
			std::cout << "Generation time of encoding: " << cpuTime() - start_time << " s." << std::endl;
		}

		if(params.count("dump-layers"))
		{
			Transalg::LogicVarContainer vars;
			const Transalg::LogicVarContainer& in_vars(encode->input_vars);
			const Transalg::LogicVarContainer& sys_vars(encode->tseitin_vars);
			const Transalg::LogicVarContainer& out_vars(encode->output_vars);
			vars.insert(vars.end(), in_vars.begin(), in_vars.end());
			vars.insert(vars.end(), sys_vars.begin(), sys_vars.end());
			vars.insert(vars.end(), out_vars.begin(), out_vars.end());

			Transalg::VarLayers var_layers(vars);

			// Переменные по слоям
			{
				std::ofstream out("var_layers.txt", std::ios::out);
				if(!out.is_open())
					throw std::runtime_error("main: can't open file var_layers.txt.");
				var_layers.WriteOutLayers(out);
			}

			// Порядок угадывания
			{
				std::ofstream out("var_assigns.txt", std::ios::out);
				if(!out.is_open())
					throw std::runtime_error("main: can't open file var_assigns.txt.");
				var_layers.WriteOutVarAssigns(out);
			}

			// Дистанции между уровнями
			{
				std::ofstream out("layer_distance.txt", std::ios::out);
				if(!out.is_open())
					throw std::runtime_error("main: can't open file layer_distance.txt.");
				var_layers.WriteOutLayersDistance(out);
			}
		}
	}
	catch (const Transalg::ParseError& e)
	{
		std::cerr << "PARSE ERROR: " << e.what() << std::endl;
		return 1;
	}
	catch (const std::exception& e)
	{
		std::cerr << "ERROR: " << e.what() << std::endl;
		return 1;
	}

	return 0;
}
