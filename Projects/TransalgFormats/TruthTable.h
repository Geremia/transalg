#pragma once

namespace Transalg
{

//! Таблица истинности дискретной функции
class TruthTable
{
public:
	typedef unsigned int Data;
	typedef std::pair<Data, Data> Row;
	typedef std::map<Data, Data> RowContainer;

	TruthTable(std::string id, std::size_t input_size, std::size_t output_size);

	const std::string& GetId() const;

	//! Число входных переменных дискретной функции
	std::size_t GetInputSize() const;

	//! Число выходных переменных дискретной функции
	std::size_t GetOutputSize() const;

	//! Число строк в таблице (храним только те строки, в которых значение функции отлично от 0)
	std::size_t GetTableSize() const;

	RowContainer::const_iterator Begin() const;

	RowContainer::const_iterator End() const;

	Data GetValue(Data input) const;

private:

	std::string id_;
	//! Число входов
	std::size_t input_size_;
	//! Число выходов
	std::size_t output_size_;
	//! Содержание табл. истинности
	//! Храним только те строки, в которых значение функции отлично от 0
	RowContainer table_;
};

typedef boost::shared_ptr<TruthTable> TruthTablePtr;

} // namespace Transalg
