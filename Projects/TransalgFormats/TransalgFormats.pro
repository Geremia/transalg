TEMPLATE = lib
TARGET = TransalgFormats

# Здесь подключаем boost и в будущем другие сторонние библиотеки
include(../../Mk/Common/common.pri)

CONFIG += staticlib

CONFIG(debug, debug|release) {
	OBJECTS_DIR += "$$BASEPATH/$$OBJ_FOLDER/$$DEBUG_FOLDER/$$TARGET"
	DESTDIR = "$$BASEPATH/$$LIB_FOLDER/$$DEBUG_FOLDER"
	LIBS += -L"$$BASEPATH/$$LIB_FOLDER/$$DEBUG_FOLDER"
} else {
	OBJECTS_DIR += "$$BASEPATH/$$OBJ_FOLDER/$$RELEASE_FOLDER/$$TARGET"
	DESTDIR = "$$BASEPATH/$$LIB_FOLDER/$$RELEASE_FOLDER"
	LIBS += -L"$$BASEPATH/$$LIB_FOLDER/$$RELEASE_FOLDER"
}

# Дополнительные каталоги включения для данного проекта
INCLUDEPATH += src . ..

PRECOMPILED_HEADER = src/stdafx.h
PRECOMPILED_SOURCE = src/stdafx.cpp

HEADERS += \
	EncodeFactory.h \
	LogicEquationSerializer.h \
	OutputGenerator.h \
	TruthTable.h \
	Types.h \
	src/EspressoUtils.h

SOURCES += \
	src/CheckEncode.cpp \
	src/EncodeFactory.cpp \
	src/EspressoUtils.cpp \
	src/LogicEquationSerializer.cpp \
	src/OutputGenerator.cpp \
	src/TruthTable.cpp \
	src/Types.cpp

# Отладочная информация

#!build_pass:message("Config: $$CONFIG")
#!build_pass:message("Base path: $$BASEPATH")
#!build_pass:message("Boost path: $$BOOST_PATH")
#!build_pass:message("Include paths: $$INCLUDEPATH")
#!build_pass:message("Depend paths: $$DEPENDPATH")
#!build_pass:message("Object dir: $$OBJECTS_DIR")
#!build_pass:message("Libs paths: $$LIBS")
#!build_pass:message("Destdir: $$DESTDIR")
