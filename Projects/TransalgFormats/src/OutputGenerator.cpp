#include "stdafx.h"
#include "../OutputGenerator.h"
#include "../LogicEquationSerializer.h"
#include "EspressoUtils.h"
#include "../VarLayers.h"

namespace Transalg
{

OutputGenerator::OutputGenerator()
	: var_count_(0)
	, disjunct_count_(0)
	, literals_count_(0)
	, xor_clause_count_(0)
	, input_var_count_(0)
	, output_var_count_(0)
	, show_layers_(false)
{}

void OutputGenerator::AddComment(const std::string& comment)
{
	comments_.push_back(comment);
}

void OutputGenerator::AddComment(const char* comment)
{
	comments_.push_back(std::string(comment));
}

void OutputGenerator::AddEncode(const LogicVarContainer& vars)
{
	encode_vars_.insert(encode_vars_.end(), vars.begin(), vars.end());
}

void OutputGenerator::AddEncode(const LogicFormulaContainer& constraints)
{
	encode_constraints_.insert(encode_constraints_.end(), constraints.begin(), constraints.end());
}

size_t OutputGenerator::GetVarCount() const
{
	return var_count_;
}

size_t OutputGenerator::GetDisjunctCount() const
{
	return disjunct_count_;
}

size_t OutputGenerator::GetLiteralsCount() const
{
	return literals_count_;
}

void OutputGenerator::SetCoreVars(const LogicVarSet& vars)
{
	core_vars_.clear();
	for(LogicVarSet::const_iterator it = vars.begin(); it != vars.end(); ++it)
	{
		LogicVarPtr var(*it);
		if(!var) continue;
		core_vars_.insert(var->var_id);
	}
}

void OutputGenerator::ShowLayerVars(bool show)
{
	show_layers_ = show;
}

void OutputGenerator::Reset()
{
	var_count_ = 0;
	disjunct_count_ = 0;
	literals_count_ = 0;
	xor_clause_count_ = 0;
	input_var_count_ = 0;
	output_var_count_ = 0;
}

void OutputGenerator::Generate(const std::string& filename, OutputFormat format, bool xor_encode)
{
	//! Сбрасываем счетчики переменных и ограничений
	Reset();
	//! Флаг, включающий кодирование xor-ограничений
	xor_encode_ = xor_encode;
	var_count_ = encode_vars_.size();

	//! Записываем файл с КНФ в DIMACS-формате
	std::ofstream out(filename.c_str(), std::ios::out);
	if(!out.is_open())
		throw std::runtime_error("OutputGenerator::Generate(): Can't open file " + filename);

	if(format == Transalg::OutputFormatCnf)
	{
		WriteOutCnf(out);
	}
	else if(format == Transalg::OutputFormatLs)
	{
		WriteOutLs(out);
	}
	else
		throw std::invalid_argument("OutputGenerator::Generate(): unsupported output format!");

	out.close();
}

void OutputGenerator::WriteOutCnf(std::ostream& out)
{
	std::size_t i;
	std::stringstream code;

	//! Определения переменных кода в порядке вход - промежуточные - выход
	for(i = 0; i < encode_vars_.size(); ++i)
	{
		WriteOutCnf(code, encode_vars_[i]);
	}
	//! Ограничения над переменными кода
	for(i = 0; i < encode_constraints_.size(); ++i)
	{
		WriteOutCnf(code, encode_constraints_[i]);
	}

	//! Выводим комментарии в шапке
	out << "p cnf " << var_count_ << " " << disjunct_count_ << std::endl;
	for(unsigned i = 0; i < comments_.size(); ++i)
		out << "c " << comments_[i] << std::endl;

	//! Число переменных, кодирующих вход
	out << "c input variables " << input_var_count_ << std::endl;

	//! Число переменных, кодирующих выход
	out << "c output variables " << output_var_count_ << std::endl;

	//! Число литералов в формуле
	out << "c literals " << literals_count_ << std::endl;

	//! Переменные ядра, если есть
	if(!core_vars_.empty())
	{
		out << "c var_set";
		for(std::set<VarId>::const_iterator it = core_vars_.begin();
			it != core_vars_.end();
			++it)
		{
			out << " " << *it;
		}
		out << std::endl;
	}

	//! Переменные слоев, если необходимо
	if(show_layers_)
	{
		VarLayers var_layers(encode_vars_);
		var_layers.WriteLayersDimacs(out);
	}

	//! Выводим тело КНФ
	out << code.rdbuf();
}

void OutputGenerator::WriteOutLs(std::ostream& out) const
{
	LogicEquationSerializer serializer(out);
	out << "Core vars:" << std::endl;
	serializer.WriteOut(core_vars_);
	out << std::endl << "Encode vars:" << std::endl;
	serializer.WriteOut(encode_vars_);
	out << std::endl << "Constraints:" << std::endl;
	serializer.WriteOut(encode_constraints_);
}

void OutputGenerator::WriteOutCnf(std::ostream& out, const LogicFormulaPtr& formula)
{
	if(!formula || !formula->size())
	{
		LOG_WARNING("OutputGenerator::WriteOutCnf: formula - null pointer.");
		return;
	}
	if(formula->IsConst())
	{
		LOG_WARNING("OutputGenerator::WriteOutCnf: formula - logical constant " + 
			std::string(formula->front().value.True() ? "1." : "0."));
		return;
	}
	//! Рассмотрим частные случаи
	//! case 1: формула - это одна переменная в положительной фазе
	if(formula->IsVariable())
	{
		out << formula->front().var->var_id << " 0" << std::endl;
		disjunct_count_++;
		literals_count_++;
		return;
	}

	if(formula->IsFunction())
	{
		VarValueMap var_value_map;
		formula->GetVariableMap(var_value_map);

		//! Кодирование линейных функций одним xor-ограничением
		if(xor_encode_ && formula->IsLinearFunction())
		{
			for(VarValueMap::const_iterator it(var_value_map.begin());
				it != var_value_map.end();
				++it)
			{
				out << " " << it->first;
				++literals_count_;
			}
			out << " 0\n";
			++xor_clause_count_;
			++disjunct_count_;
			return;
		}

		//! Минимизация формулы при помощи Espresso
		{
			const std::size_t table_cols = var_value_map.size();
			const std::size_t table_rows = static_cast<std::size_t>(1) << table_cols;
			Table table_in(table_rows, table_cols);
			std::size_t row(0), col(0);
			for(unsigned values = 0; values < table_rows; ++values)
			{
				SetVarValues(var_value_map, values);
				LogicValueType value = formula->ComputeValue(var_value_map);
				if(value.True())
				{
					col = 0;
					for(VarValueMap::const_iterator it(var_value_map.begin());
						it != var_value_map.end();
						++it, ++col)
					{
						table_in.data[row][col] = (it->second.True() ? 1 : 0);
					}
					++row;
				}
			}
			//! Удалить лишние строки
			table_in.ResetRows(row);

			//! Минимизируем формулу при помощи Espresso
			Table table_out;
			if(int code = Minimize(table_in, table_out))
			{
				std::cerr << "Minimize result code: " << code << std::endl;
				return;
			}

			//! Номера переменных
			std::vector<unsigned> var_numbers;
			var_numbers.reserve(var_value_map.size());
			for(VarValueMap::const_iterator it(var_value_map.begin());
				it != var_value_map.end();
				++it)
			{
				var_numbers.push_back(it->first);
			}

			//! Выводим полученную формулу в DIMACS формате
			WriteCnfDimacs(out, table_out, var_numbers, disjunct_count_, literals_count_);
		}

	}
}

void OutputGenerator::WriteOutCnf(std::ostream& out, const LogicVarPtr& var)
{
	if(!var)
	{
		LOG_ERROR("OutputGenerator::WriteOutCnf: var - null pointer.");
		return;
	}

	if(var->var_type == LogicVarInput)
	{
		++input_var_count_;
		if(!var->var_value)
			return;
	}
	
	if(var->var_type == LogicVarOutput)
	{
		++output_var_count_;
	}

	if(!var->var_value)
	{
		LOG_WARNING("OutputGenerator::WriteOutCnf: var_value - null pointer.");
		return;
	}

	const LogicFormulaPtr& var_value = var->var_value;

	//! Рассмотрим частные случаи
	//! case 1: значение переменной кода известно
	if(var_value->IsConst())
	{
		assert(var_value->GetValue() != Transalg::LogicValueUndef);
		out << (var_value->GetValue() == Transalg::LogicValueTrue ? "" : "-");
		out << var->var_id << " 0" << std::endl;
		disjunct_count_++;
		literals_count_++;
		return;
	}

	//! case 2: переменная кода эквивалентна другой переменной кода
	if(var_value->IsVariable())
	{
		const LogicVarPtr& var_right = var_value->front().var;
		out << "-" << var->var_id << " " << var_right->var_id << " 0" << std::endl;
		out << var->var_id << " -" << var_right->var_id << " 0" << std::endl;
		disjunct_count_ += 2;
		literals_count_ += 4;
		return;
	}

	//! case 3: переменная кода эквивалентная некоторой булевой функции
	if(var_value->IsFunction())
	{
		VarValueMap var_value_map;
		var_value->GetVariableMap(var_value_map);

		//! Кодирование линейных функций одним xor-ограничением
		if(xor_encode_ && var_value->IsLinearFunction())
		{
			out << "xor -" << var->var_id;
			for(VarValueMap::const_iterator it(var_value_map.begin());
				it != var_value_map.end();
				++it)
			{
				out << " " << it->first;
				++literals_count_;
			}
			out << " 0\n";
			++xor_clause_count_;
			++disjunct_count_;
			return;
		}

		//! Минимизация формулы при помощи Espresso
		{
			//! Создадим таблицы истинности 
			//! (включаем в нее только те строки, на которых формула принимает значение истина)
			const std::size_t table_cols = var_value_map.size() + 1;
			const std::size_t table_rows = static_cast<std::size_t>(1) << (table_cols - 1);
			Table table_in(table_rows, table_cols);
			for(size_t row = 0; row < table_rows; ++row)
			{
				SetVarValues(var_value_map, row);
				LogicValueType value = var_value->ComputeValue(var_value_map);
				//! Значение переменной равно значению формулы, через которую определяется данная переменная
				table_in.data[row][0] = value.True() ? 1 : 0;
				size_t col(1);
				for(VarValueMap::const_iterator it(var_value_map.begin());
					it != var_value_map.end();
					++it, ++col)
				{
					table_in.data[row][col] = (it->second.True() ? 1 : 0);
				}
			}

			//! Минимизируем формулу при помощи Espresso
			Table table_out;
			if(int code = Minimize(table_in, table_out))
			{
				std::cerr << "Minimize result code: " << code << std::endl;
				return;
			}

			//! Номера переменных
			std::vector<unsigned> var_numbers;
			var_numbers.reserve(table_cols);
			var_numbers.push_back(var->var_id);
			for(VarValueMap::const_iterator it(var_value_map.begin());
				it != var_value_map.end();
				++it)
			{
				var_numbers.push_back(it->first);
			}

			//! Выводим полученную формулу в DIMACS формате
			WriteCnfDimacs(out, table_out, var_numbers, disjunct_count_, literals_count_);
		}
	}
}

void OutputGenerator::SetVarValues(VarValueMap& var_value_map, unsigned values) const
{
	int i(0);
	for(VarValueMap::iterator it(var_value_map.begin());
		it != var_value_map.end();
		++it)
	{
		it->second = ((values & (1 << i++)) ? LogicValueTrue : LogicValueFalse);
	}
}

} // namespace Transalg
