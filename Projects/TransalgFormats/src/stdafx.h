#pragma once

#include <stdexcept>
#include <cassert>
#include <vector>
#include <map>
#include <set>
#include <stack>
#include <string>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <iomanip>

#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/lexical_cast.hpp>

//! Utils
#include <TransalgUtils/Logger.h>
