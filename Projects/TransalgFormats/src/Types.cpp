#include "stdafx.h"
#include "../Types.h"

namespace Transalg
{

LogicValueType::LogicValueType(unsigned char v)
	: value(v > 2 ? 2 : v)
{
}

bool LogicValueType::operator==(const LogicValueType& b) const
{
	return value == b.value;
}

bool LogicValueType::operator!=(const LogicValueType& b) const
{
	return value != b.value;
}

LogicValueType LogicValueType::operator&&(const LogicValueType& b) const
{
	if(value == 2 || b.value == 2)
		return LogicValueUndef;
	return LogicValueType(value & b.value);
}

LogicValueType LogicValueType::operator||(const LogicValueType& b) const
{
	if(value == 2 || b.value == 2)
		return LogicValueUndef;
	return LogicValueType(value | b.value);
}

LogicValueType LogicValueType::operator^ (const LogicValueType& b) const
{
	if(value == 2 || b.value == 2)
		return LogicValueUndef;
	return LogicValueType(value ^ b.value);
}

LogicValueType LogicValueType::operator! () const
{
	return (value == 2 ? LogicValueUndef : LogicValueType(value ^ 1));
}

LogicValueType LogicValueType::Equiv(const LogicValueType& b) const
{
	return value == b.value ? LogicValueTrue : LogicValueFalse;
}

///////////////////////////////////////////////////////////////////////////////
// class LogicOperand

LogicOperand::LogicOperand(LogicValueType value)
	: type(LogicOperandConst)
	, value(value)
	, func_type(LogicFunctionUndef)
	, arg_count(0)
{}

LogicOperand::LogicOperand(LogicVarPtr var)
	: type(LogicOperandVariable)
	, value(LogicValueUndef)
	, func_type(LogicFunctionUndef)
	, arg_count(0)
	, var(var)
{}

LogicOperand::LogicOperand(LogicFunctionType ftype, TruthTablePtr table_ptr)
	: type(LogicOperandFunction)
	, value(LogicValueUndef)
	, func_type(ftype)
	, arg_count(0)
	, truth_table(table_ptr)
{
	switch(func_type)
	{
	case Transalg::LogicFunctionNot: 
		arg_count = 1; 
		break;
	case Transalg::LogicFunctionAnd:
	case Transalg::LogicFunctionOr:
	case Transalg::LogicFunctionXor:
	case Transalg::LogicFunctionEquiv:
		arg_count = 2;
		break;
	}
}

///////////////////////////////////////////////////////////////////////////////
// class LogicFormula

LogicFormula::LogicFormula(LogicValueType value)
{
	push_back(LogicOperand(value));
}

LogicFormula::LogicFormula(LogicVarPtr var)
{
	push_back(LogicOperand(var));
}

LogicFormula::LogicFormula(const LogicOperand& operand)
{
	unique_var_count_ = (operand.type == Transalg::LogicOperandVariable ? 1 : 0);
	push_back(operand);
}

LogicFormula::LogicFormula(const LogicOperandContainer& operands)
{
	insert(end(), operands.begin(), operands.end());
	std::set<VarId> var_ids;
	for(LogicFormula::const_iterator it = begin(); it != end(); ++it)
	{
		const LogicOperand& op(*it);
		if(op.type == Transalg::LogicOperandVariable)
		{
			assert(op.var);
			var_ids.insert(op.var->var_id);
		}
	}
	unique_var_count_ = var_ids.size();
}

bool LogicFormula::IsLinearFunction() const
{
	bool is_linear(false);
	for(LogicFormula::const_iterator it = begin(); it != end(); ++it)
	{
		const LogicOperand& op(*it);
		if(op.type == Transalg::LogicOperandFunction)
		{
			if(op.func_type == Transalg::LogicFunctionXor)
				is_linear = true;
			else if(op.func_type == Transalg::LogicFunctionNot)
				continue;
			else
				return false;
		}
	}
	return is_linear;
}

//! Получить множество переменных, над которыми построена формула
void LogicFormula::GetVariableSet(LogicVarSet& vars) const
{
	vars.clear();
	for(LogicFormula::const_iterator it = begin(); it != end(); ++it)
	{
		const LogicOperand& op(*it);
		if(op.type == Transalg::LogicOperandVariable)
		{
			vars.insert(op.var);
		}
	}
}

void LogicFormula::GetVariableMap(VarValueMap& vars) const
{
	vars.clear();
	for(LogicFormula::const_iterator it = begin(); it != end(); ++it)
	{
		if(it->type == Transalg::LogicOperandVariable)
		{
			vars.insert(std::make_pair(it->var->var_id, LogicValueFalse));
		}
	}
}

//! Вычислить значение выражения на переданном наборе значений истинности переменных
LogicValueType LogicFormula::ComputeValue(const VarValueMap& value_map) const
{
	//! Для константы сразу возвращаем значение
	if(IsConst())
	{
		return front().value;
	}
	//! Значение переменной берем из переданной мапы
	if(IsVariable())
	{
		if(!front().var)
			throw std::runtime_error("LogicFormula::ComputeValue: var - null pointer.");
		VarValueMap::const_iterator vit = value_map.find(front().var->var_id);
		return (vit == value_map.end() ? LogicValueUndef : vit->second);
	}
	//! Для функции пытаем вычислить значение 
	if(IsFunction())
	{
		std::stack<LogicValueType> mag;
		for(LogicFormula::const_iterator it = begin(); it != end(); ++it)
		{
			const LogicOperand& op(*it);
			//! Помещаем в стек значение булевой константы
			if(op.type == Transalg::LogicOperandConst)
			{
				assert(op.value != LogicValueUndef);
				mag.push(op.value);
			}
			//! Помещаем в стек значение булевой переменной
			else if(op.type == Transalg::LogicOperandVariable)
			{
				VarValueMap::const_iterator vit = value_map.find(op.var->var_id);
				if(vit == value_map.end())
					return LogicValueUndef;
				assert(vit->second != LogicValueUndef);
				mag.push(vit->second);
			}
			//! Вычисляем на стеке значение функции, результат помещаем в стек
			else if(op.type == Transalg::LogicOperandFunction)
			{
				CalculateFunction(mag, op);
			}
			else
				throw std::runtime_error("LogicFormula::ComputeValue: unknown type of logic operand.");
		}
		assert(mag.size() == 1);
		return mag.top();
	}
	return LogicValueUndef;
}

LogicValueType LogicFormula::GetValue() const
{
	//! Для константы или переменной сразу возвращаем значение
	if(IsConst() || IsVariable())
	{
		return front().value;
	}
	//! Для функции пытаем вычислить значение 
	if(IsFunction())
	{
		std::stack<LogicValueType> mag;
		for(LogicFormula::const_iterator it = begin(); it != end(); ++it)
		{
			const LogicOperand& op(*it);
			//! Помещаем в стек значение булевой константы
			if(op.type == Transalg::LogicOperandConst)
			{
				assert(op.value != LogicValueUndef);
				mag.push(op.value);
			}
			//! Помещаем в стек значение булевой переменной
			else if(op.type == Transalg::LogicOperandVariable)
			{
				if(op.value == LogicValueUndef)
					return LogicValueUndef;
				mag.push(op.value);
			}
			//! Вычисляем на стеке значение функции, результат помещаем в стек
			else if(op.type == Transalg::LogicOperandFunction)
			{
				CalculateFunction(mag, op);
			}
			else
				throw std::runtime_error("LogicFormula::GetValue: unknown type of logic operand.");
		}
		assert(mag.size() == 1);
		return mag.top();
	}
	return LogicValueUndef;
}

LogicValueType LogicFormula::CalculateFunction(std::stack<LogicValueType>& mag, const LogicOperand& func)
{
	if(func.type != Transalg::LogicOperandFunction)
		throw std::invalid_argument("LogicFormula::CalculateFunction: func - is not function operand.");

	switch(func.func_type)
	{
	case Transalg::LogicFunctionNot:
		{
			const LogicValueType arg = mag.top(); mag.pop();
			assert(arg != LogicValueUndef);
			mag.push(!arg);
		}
		break;
	case Transalg::LogicFunctionAnd:
		{
			const LogicValueType arg1 = mag.top(); mag.pop();
			const LogicValueType arg2 = mag.top(); mag.pop();
			assert(arg1 != LogicValueUndef);
			assert(arg2 != LogicValueUndef);
			mag.push(arg1 && arg2);
		}
		break;
	case Transalg::LogicFunctionOr:
		{
			const LogicValueType arg1 = mag.top(); mag.pop();
			const LogicValueType arg2 = mag.top(); mag.pop();
			assert(arg1 != LogicValueUndef);
			assert(arg2 != LogicValueUndef);
			mag.push(arg1 || arg2);
		}
		break;
	case Transalg::LogicFunctionXor:
		{
			const LogicValueType arg1 = mag.top(); mag.pop();
			const LogicValueType arg2 = mag.top(); mag.pop();
			assert(arg1 != LogicValueUndef);
			assert(arg2 != LogicValueUndef);
			mag.push(arg1 ^ arg2);
		}
		break;
	case Transalg::LogicFunctionEquiv:
		{
			const LogicValueType arg1 = mag.top(); mag.pop();
			const LogicValueType arg2 = mag.top(); mag.pop();
			assert(arg1 != LogicValueUndef);
			assert(arg2 != LogicValueUndef);
			mag.push(arg1.Equiv(arg2));
		}
		break;
	case Transalg::LogicFunctionTable:
		{
			throw std::invalid_argument("LogicFormula::CalculateFunction: truth table is not implemented.");
		}
		break;
	default:
		{
			throw std::invalid_argument("LogicFormula::CalculateFunction: undefined type of function.");
		}
	}
	return mag.top();
}

bool LogicFormula::IsFunction() const
{
	return (size() <= 1 ? false : back().type == Transalg::LogicOperandFunction);
}

bool LogicFormula::IsConst() const
{
	return (size() != 1 ? false : front().type == Transalg::LogicOperandConst);
}

bool LogicFormula::IsVariable() const
{
	return (size() != 1 ? false : front().type == Transalg::LogicOperandVariable);
}

void LogicFormula::Normalize()
{
	//! Сократим идущие подряд операторы отрицания (если они есть)
	for(size_t i(0); i < size(); ++i)
	{
		const LogicOperand& op(this->operator[](i));
		if (op.type == Transalg::LogicOperandFunction && 
			op.func_type == Transalg::LogicFunctionNot)
		{
			size_t j(i + 1);
			for(; j < size(); ++j)
			{
				const LogicOperand& op2(this->operator[](j));
				if (!(op.type == Transalg::LogicOperandFunction && 
					op.func_type == Transalg::LogicFunctionNot))
				{
					break;
				}
			}
			//! Если подряд идущих отрицаний больше 1, то сокращаем их 
			if (j - i > 1)
			{
				//! Число отрицаний нечетное - одно оставляем
				if((j - i)%2 != 0)
				{
					++i;
				}
				const size_t shrink_size(j - i);
				assert(shrink_size >= 2);
				for(size_t k(i); j < size(); ++j, ++k)
				{
					this->operator[](k) = this->operator[](j);
				}
				resize(size() - shrink_size);
			}
		}
	}
}

bool LogicFormula::operator== (const LogicFormula& x) const
{
	if(size() != x.size())
		return false;

	for(unsigned i(0); i < size(); ++i)
	{
		if((*this)[i] != x[i])
			return false;
	}
	return true;
}

bool LogicFormula::operator!= (const LogicFormula& x) const
{
	return !(*this == x);
}

bool LogicVar::operator==(const LogicVar& a) const
{
	return (var_id == a.var_id) && (var_type == a.var_type);
}

bool LogicVar::operator!=(const LogicVar& a) const
{
	return !(*this == a);
}

bool LogicOperand::operator==(const LogicOperand& a) const
{
	return (type == a.type) &&
		(value == a.value) && 
		(var == a.var) &&
		(func_type == a.func_type) &&
		(arg_count == a.arg_count);
}

bool LogicOperand::operator!=(const LogicOperand& a) const
{
	return !(*this == a);
}

std::ostream& operator<<(std::ostream& out, const LogicOperand& x)
{
	switch(x.type)
	{
	case LogicOperandConst:
		out << x.value.True() ? "1" : x.value.False() ? "0" : "?";
		break;
	case LogicOperandVariable:
		out << "x" << x.var->var_id;
		break;
	case LogicOperandFunction:
		{
			switch(x.func_type)
			{
			case LogicFunctionNot: out << "!"; break;
			case LogicFunctionAnd: out << "&"; break;
			case LogicFunctionOr:  out << "|"; break;
			case LogicFunctionXor: out << "^"; break;
			case LogicFunctionEquiv: out << "="; break;
			// case LogicFunctionTable: out << x.truth_table->GetId(); break;
			default: out << "?";
			}
		}
		break;
	default:
		out << "?";
	}
	return out;
}

std::ostream& operator<<(std::ostream& out, const LogicFormula& x)
{
	for(LogicFormula::const_iterator it = x.begin(); it != x.end(); ++it)
	{
		out << (it != x.begin() ? " " : "") << *it;
	}
	return out;
}

std::ostream& operator<<(std::ostream& out, const LogicVar& x)
{
	out << "x" << x.var_id << " = ";
	if(!x.var_value)
		out << "?";
	else
		out << *x.var_value;
	out << std::endl;
	return out;
}

} // namespace Transalg
