#pragma once

namespace Transalg
{

//! Типы логических формул
enum LogicOperandType
{
	//! Абстрактная булева формула
	LogicOperandFormula,
	//! Булева константа
	LogicOperandConst,
	//! Булева переменная
	LogicOperandVariable,
	//! Булева/дискретная функция
	LogicOperandFunction
};

//! Значение истинности переменной кода / булевой функции
class LogicValueType
{
	unsigned char value;
public:

	LogicValueType(unsigned char v);

	bool operator==(const LogicValueType& b) const;
	bool operator!=(const LogicValueType& b) const;
	LogicValueType operator&&(const LogicValueType& b) const;
	LogicValueType operator||(const LogicValueType& b) const;
	LogicValueType operator^ (const LogicValueType& b) const;
	LogicValueType operator! () const;
	LogicValueType Equiv(const LogicValueType& b) const;
	bool True() const {return value == 1;}
	bool False() const {return value == 0;}
	bool Undef() const {return value == 2;}
	unsigned char GetData() const {return value;}
};

const LogicValueType LogicValueFalse(0);
const LogicValueType LogicValueTrue (1);
const LogicValueType LogicValueUndef(2);

//! Тип переменной кода
enum LogicVarType
{
	//! Цейтиновская переменная
	LogicVarTseitin, 
	//! Переменная, кодирующая бит входа
	LogicVarInput, 
	//! Переменная, кодирующая бит выхода
	LogicVarOutput
};

//! Тип дискретной функции
//! Функции должны быть перечислены в порядке убывания приоритета
enum LogicFunctionType
{
	LogicFunctionUndef,
	LogicFunctionNot,
	LogicFunctionAnd,
	LogicFunctionOr,
	LogicFunctionXor,
	LogicFunctionEquiv,
	LogicFunctionTable
};

//! forward declaration
class LogicFormula;
typedef boost::shared_ptr<LogicFormula> LogicFormulaPtr;
typedef std::vector<LogicFormulaPtr> LogicFormulaContainer;
typedef boost::shared_ptr<LogicFormulaContainer> LogicFormulaContainerPtr;

class TruthTable;
typedef boost::shared_ptr<TruthTable> TruthTablePtr;

typedef unsigned int VarId;
typedef std::vector<VarId> VarIdContainer;
typedef boost::shared_ptr<VarIdContainer> VarIdContainerPtr;

//! Описание переменной кода
struct LogicVar
{
	LogicVar(VarId id, LogicVarType type = LogicVarTseitin, LogicFormulaPtr value = LogicFormulaPtr())
		: var_id(id)
		, var_type(type)
		, var_value(value)
	{}

	LogicVarType var_type;
	VarId var_id;
	LogicFormulaPtr var_value;

	bool operator==(const LogicVar& a) const;
	bool operator!=(const LogicVar& a) const;
	friend std::ostream& operator<<(std::ostream& out, const LogicVar& x);
};

typedef boost::shared_ptr<LogicVar> LogicVarPtr;
typedef std::vector<LogicVarPtr> LogicVarContainer;
typedef boost::shared_ptr<LogicVarContainer> LogicVarContainerPtr;

typedef std::set<LogicVarPtr> LogicVarSet;
typedef std::map<VarId, LogicVarPtr> LogicVarMap;
typedef std::map<VarId, LogicValueType> VarValueMap;

//! Операнд логической формулы
struct LogicOperand
{
	LogicOperand(LogicOperandType type = LogicOperandFormula)
		: type(type)
		, value(LogicValueUndef)
		, func_type(LogicFunctionUndef)
		, arg_count(0)
	{}

	LogicOperand(LogicValueType value);
	LogicOperand(LogicVarPtr var);
	LogicOperand(LogicFunctionType type, TruthTablePtr table_ptr = TruthTablePtr());

	//! Тип операнда
	LogicOperandType type;

	//! Значение константы, если известно
	LogicValueType value;

	//! Указатель на переменную кода, если операндом является переменная
	LogicVarPtr var;

	//! Если операндом является функция, то здесь вся информация по ней
	LogicFunctionType func_type;
	std::size_t arg_count;

	TruthTablePtr truth_table;

	bool operator==(const LogicOperand& a) const;
	bool operator!=(const LogicOperand& a) const;
	friend std::ostream& operator<<(std::ostream& out, const LogicOperand& x);
};

typedef std::vector<LogicOperand> LogicOperandContainer;
typedef boost::shared_ptr<LogicOperandContainer> LogicOperandContainerPtr;

class LogicFormula: public LogicOperandContainer
{
public:

	LogicFormula(): unique_var_count_(0) {}
	LogicFormula(LogicValueType value);
	LogicFormula(LogicVarPtr var);
	LogicFormula(const LogicOperand& operand);
	LogicFormula(const LogicOperandContainer& operands);

	bool IsLinearFunction() const;

	bool IsFunction() const;
	bool IsConst() const;
	bool IsVariable() const;

	/**
	* @brief Выполнить нормализацию формулы - сокращаем идущие подряд операции отрицания.
	* to do: коммутативных операциях переменные сортируются по порядку.
	*/
	void Normalize();

	size_t HashCode() const
	{
		size_t seed(0);
		for(LogicFormula::const_iterator it = begin(); it != end(); ++it)
		{
			const LogicOperand& op(*it);
			boost::hash_combine(seed, static_cast<size_t>(op.type));
			boost::hash_combine(seed, op.value.GetData());
			boost::hash_combine(seed, op.var.get());
			boost::hash_combine(seed, static_cast<size_t>(op.func_type));
			boost::hash_combine(seed, op.arg_count);
			boost::hash_combine(seed, op.truth_table.get());
		}
		return seed;
	}

	//! Получить множество переменных, над которыми построена формула
	void GetVariableSet(LogicVarSet& vars) const;

	void GetVariableMap(VarValueMap& vars) const;

	//! Текущее значение формулы
	LogicValueType GetValue() const;

	//! Вычислить значение выражения на переданном наборе значений истиности переменных
	LogicValueType ComputeValue(const VarValueMap& value_map) const;

	//! Вычисление функции на стеке
	static LogicValueType CalculateFunction(std::stack<LogicValueType>& mag, const LogicOperand& func);

	bool operator== (const LogicFormula& x) const;
	bool operator!= (const LogicFormula& x) const;
	friend std::ostream& operator<<(std::ostream& out, const LogicFormula& x);

private:

	//! Число уникальных переменных, над которыми построена формула
	std::size_t unique_var_count_;
};

} // namespace Transalg
